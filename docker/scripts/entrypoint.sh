#!/bin/bash
# -----------------------------------------------------------------------------
# docker-pinry /start script
#
# Will setup database and static files if they don't exist already, if they do
# just continues to run docker-pinry.
#
# Authors: Isaac Bythewood
# Updated: Aug 19th, 2014
# -----------------------------------------------------------------------------
PROJECT_ROOT="/pinry"
export DJANGO_SETTINGS_MODULE=pinry.settings.docker

mkdir -p /data/static/media
mkdir -p /data/pinry-spa

rm -rf /data/pinry-spa/*
cp -R /pinry/pinry-spa/dist /data/pinry-spa/

cd ${PROJECT_ROOT} || exit 1
poetry run python manage.py migrate
poetry run python manage.py collectstatic --noinput

exec "$@"
