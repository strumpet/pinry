# -----------------------------------------------------------------------------
# docker-pinry
#
# Builds a docker image that can run Pinry (http://getpinry.com)
#
# Authors: Isaac Bythewood, Jason Kaltsikis
# Updated: Jan 21 2020
# Require: Docker (http://www.docker.io/)
# -----------------------------------------------------------------------------

FROM node:16-alpine as yarn-build

WORKDIR pinry-spa
COPY pinry-spa/package.json pinry-spa/yarn.lock ./
RUN yarn install
COPY pinry-spa .
RUN yarn build


FROM python:3.9.16-slim

RUN apt-get -yq update && \
  apt-get -yq upgrade && \
  DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install \
        build-essential \
        libjpeg-dev \
        libpq-dev \
        zlib1g-dev \
  && apt-get -y clean && \
  rm -rf /var/lib/apt/lists/*

WORKDIR /pinry

COPY pyproject.toml ./
COPY poetry.lock ./
RUN pip install rcssmin \
    && pip install poetry \
    && poetry install

COPY . .

COPY --from=yarn-build pinry-spa/dist /pinry/pinry-spa/dist

ENTRYPOINT ["/pinry/docker/scripts/entrypoint.sh"]
CMD        ["/pinry/docker/scripts/_start_gunicorn.sh"]
